#define dirPinR 6
#define speedPinR 3
#define dirPinL 4
#define speedPinL 5


#define FORWARD 1
#define BACKWARD -1

void Init_motor();
void Set_MotorR(int R_dir, byte R_speed);
void Set_MotorL(int L_dir, byte L_speed);
void Stop_MotorR();
void Stop_MotorL();

int base_rpm=140
