void Init_motor(){
  pinMode(dirPinR, OUTPUT);
  pinMode(speedPinR, OUTPUT);
  pinMode(dirPinL, OUTPUT);
  pinMode(speedPinL, OUTPUT);
}

void Set_MotorR(int R_dir, byte R_speed){
  if(R_speed==0){
    Stop_MotorR();
  } else{
    if(R_dir == FORWARD){
      analogWrite(speedPinR, R_speed);
      digitalWrite(dirPinR, LOW);
    }else {
      analogWrite(speedPinR, 255-R_speed);
      digitalWrite(dirPinR, HIGH);
    }
  }
}

void Set_MotorL(int L_dir, byte L_speed){
  if(L_speed==0){
    Stop_MotorL();
  } else{
    if(L_dir == FORWARD){
      analogWrite(speedPinL, L_speed);
      digitalWrite(dirPinL, LOW);
    }else {
      analogWrite(speedPinL, 255-L_speed);
      digitalWrite(dirPinL, HIGH);
    }
  }
}

void Stop_MotorR(){
  digitalWrite(dirPinR, HIGH);
  digitalWrite(speedPinR, HIGH);
}

void Stop_MotorL(){
  digitalWrite(dirPinL, HIGH);
  digitalWrite(speedPinL, HIGH);
}
