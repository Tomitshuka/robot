void InitTimer5(){
  TCCR5A =0;
  TCCR5B =0;
  TCNT5 = 65535 - 20000; //10Hz //65535 - 20000; //10Hz volt
  TCCR5B |= B00000010;
  TIMSK5 |= (1<<TOIE5);//Engedélyezzük a túlcsorduláshoz való interruptot
}

ISR(TIMER5_OVF_vect){
  if(start){
     IR_read=true;

    int rpm = LineTrackingControl(position, 20);
    int R_rpm = base_rpm + rpm;
    if(R_rpm <0) R_rpm =0;
    if(R_rpm >255) R_rpm = 255;
    int L_rpm = base_rpm-rpm;
    if(L_rpm <0) L_rpm =0;
    if(L_rpm > 255) L_rpm = 255;

    Set_MotorR(FORWARD, R_rpm);
    Set_MotorL(FORWARD, L_rpm);

    TCNT5=65535-20000;  
  } else {
    Stop_MotorR();
    Stop_MotorL();
  }
 
}
