#include <Arduino.h>

const int AIA = 6; // IA1
const int AIB = 7; // IB1
const int BIA = 5; // IA2
const int BIB = 4; // IA2

byte speed = 150;

void forward()
{
  analogWrite(AIA, 0);
  analogWrite(AIB, speed);
  analogWrite(BIA, 0);
  analogWrite(BIB, speed);
}

void left()
{
  analogWrite(AIA, speed);
  analogWrite(AIB, 0);
  analogWrite(BIA, speed);
  analogWrite(BIB, 0);
}

void forward()
{
  analogWrite(AIA, speed);
  analogWrite(AIB, 0);
  analogWrite(BIA, 0);
  analogWrite(BIB, speed);
}

void backward()
{
  analogWrite(AIA, 0);
  analogWrite(AIB, speed);
  analogWrite(BIA, speed);
  analogWrite(BIB, 0);
}


void STOP()
{
  analogWrite(AIA, 0);
  analogWrite(AIB, 0);
  analogWrite(BIA, 0);
  analogWrite(BIB, 0);
  delay(2000);
}

void setup() {
  // put your setup code here, to run once:
  pinMode(AIA, OUTPUT);
  pinMode(AIB, OUTPUT);
  pinMode(BIA, OUTPUT);
  pinMode(BIB, OUTPUT);

  Serial.begin(9600);
}

void loop()
{
  Serial.println("Forward...");
  forward();
  delay(2000);
  STOP();

  Serial.println("Backward...");
  backward();
  delay(2000);
  STOP();

  Serial.println("Left...");
  left();
  delay(2000);
  STOP();

  Serial.println("Right...");
  right();
  delay(2000);
  STOP();

}