double Output;
double errSum, lastErr;
double errSum_array[] = {0,0,0,0,0,0,0,0,0,0};
double kp=5;
double ki=0;
double kd=25; //15,8

int LineTrackingControl(float actualValue, float desiredValue){
  double error = desiredValue-actualValue;
  memcpy(errSum_array, errSum_array+1, 9*sizeof(double));
  errSum_array[9] = (error);
  errSum = 0;
  for(int i=0; i<9; i++) errSum += errSum_array[i];

  double dErr=(error-lastErr);

  Output = kp*error+ki*errSum+kd*dErr;

  lastErr = error;
  return Output;
}
