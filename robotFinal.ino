#include "IR_sensor.h"
#include "Motor.h"
#include "Timer_config.h"
#include "PID.h"

void setup() {
  Init_motor();
  Init_IR_sensor();
  //Serial.begin(57600);
  Set_MotorR(FORWARD, 255);
  Set_MotorL(FORWARD, 255);
  InitTimer5();
  
}

bool start = true;

void loop() {
  // put your main code here, to run repeatedly:
  if(IR_read && start){
    IR_read=false;
    Scan();
  }
  /*
  Scan();
  Serial.println(irSensors, BIN);
  delay(50);
  */
}
